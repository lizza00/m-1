package com.m1.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
public class ResultDto {

    private List<String> categories;

    private List<String> variatees;

    private List<IterableResultDto> iterableResultDtos = new ArrayList<>();

    private String bestResult;

    private List<Double> totalScore;

    @Data
    public static class IterableResultDto {

        private String category;

        private List<List<TriangleNumber>> matrix;

        private List<TriangleNumber> r;

        private TriangleNumber sr;

        private TriangleNumber isr;

        private List<TriangleNumber> w;

        private List<Double> m;

        private List<Double> n;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        public static class TriangleNumber{
            private Double first;
            private Double mid;
            private Double second;

            @Override
            public String toString() {
                return "(" + first +
                        "; " + mid +
                        "; " + second +
                        ')';
            }
        }
    }
}
