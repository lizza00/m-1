package com.m1.service;

import com.m1.dto.ResultDto;
import com.m1.dto.ResultDto.IterableResultDto.TriangleNumber;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class CalculatingService {

    public ResultDto calculate() {
        ResultDto resultDto = new ResultDto();
        fetchVariatees(resultDto);
        fetchCategories(resultDto);
        fetchLevel(resultDto, "level-1.txt", null);
        for (int i = 1; i <= resultDto.getCategories().size(); i++) {
            fetchLevel(resultDto, "level-2-" + i + ".txt",  resultDto.getCategories().get(i-1));
        }
        fetchTotalScore(resultDto);
        System.out.println(resultDto);
        return resultDto;
    }

    private void fetchTotalScore(ResultDto resultDto){
        List<Double>scores = new ArrayList<>();
        for(int i=0;i<resultDto.getVariatees().size();i++){
            double score=0;
            for (int j=0;j<resultDto.getCategories().size();j++){
                System.out.println(resultDto.getIterableResultDtos().get(j+1).getN().get(i));
                score+=(resultDto.getIterableResultDtos().get(0).getN().get(j)*resultDto.getIterableResultDtos().get(j+1).getN().get(i));

            }
            scores.add(Math.round(score*10000)/10000.);
        }
        resultDto.setBestResult(resultDto.getVariatees().get(maxInd(scores) ));
        resultDto.setTotalScore(scores);
    }

    private int maxInd(List<Double>list){
        double asDouble = list.stream().mapToDouble(d -> d).max().getAsDouble();
        return list.indexOf(asDouble);
    }

    private void fetchVariatees(ResultDto resultDto) {
        resultDto.setVariatees(readStringListFromFile("categories-3.txt"));
    }

    private void fetchCategories(ResultDto resultDto) {
        resultDto.setCategories(readStringListFromFile("categories-2.txt"));
    }

    private void fetchLevel(ResultDto resultDto, String file, String category) {
        ResultDto.IterableResultDto iterableResultDto = new ResultDto.IterableResultDto();
        List<List<TriangleNumber>> matrix = fetchMatrix(file);
        iterableResultDto.setMatrix(matrix);
        List<TriangleNumber> rList = new ArrayList<>();
        List<TriangleNumber> wList = new ArrayList<>();
        List<Double> mList = new ArrayList<>();
        List<Double> nList = new ArrayList<>();
        TriangleNumber sr = new TriangleNumber(0.0, 0.0, 0.0);
        for (int i = 0; i < matrix.size(); i++) {
            double m1 = 1, m2 = 1, m3 = 1;
            for (int j = 0; j < matrix.get(i).size(); j++) {
                m1 *= matrix.get(i).get(j).getFirst();
                m2 *= matrix.get(i).get(j).getMid();
                m3 *= matrix.get(i).get(j).getSecond();
            }
            rList.add(new TriangleNumber(Math.round(Math.pow(m1, 1. / matrix.size()) * 10000) / 10000.,
                    Math.round(Math.pow(m2, 1. / matrix.size()) * 10000) / 10000.,
                    Math.round(Math.pow(m3, 1. / matrix.size()) * 10000) / 10000.));
            sr.setFirst(Math.round((sr.getFirst() + Math.pow(m1, 1. / matrix.size())) * 10000) / 10000.);
            sr.setMid(Math.round((sr.getMid() + Math.pow(m2, 1. / matrix.size())) * 10000) / 10000.);
            sr.setSecond(Math.round((sr.getSecond() + Math.pow(m3, 1. / matrix.size())) * 10000) / 10000.);
        }
        TriangleNumber isr = new TriangleNumber(Math.round((1. / sr.getSecond()) * 10000) / 10000.,
                Math.round((1. / sr.getMid()) * 10000) / 10000.,
                Math.round((1. / sr.getFirst()) * 10000) / 10000.);
        double sum = 0;
        for (int i = 0; i < rList.size(); i++) {
            TriangleNumber num = new TriangleNumber(Math.round(rList.get(i).getFirst() * isr.getFirst() * 10000) / 10000.,
                    Math.round(rList.get(i).getMid() * isr.getMid() * 10000) / 10000.,
                    Math.round(rList.get(i).getSecond() * isr.getSecond() * 10000) / 10000.);
            wList.add(num);
            mList.add(Math.round((num.getFirst() + num.getMid() + num.getSecond()) / 3. * 10000) / 10000.);
            sum += Math.round((num.getFirst() + num.getMid() + num.getSecond()) / 3. * 10000) / 10000.;
        }

        for (int i = 0; i < mList.size(); i++) {
            nList.add(Math.round(mList.get(i) / sum * 10000) / 10000.);
        }
        iterableResultDto.setR(rList);
        iterableResultDto.setSr(sr);
        iterableResultDto.setIsr(isr);
        iterableResultDto.setW(wList);
        iterableResultDto.setM(mList);
        iterableResultDto.setN(nList);
        iterableResultDto.setCategory(category);
        resultDto.getIterableResultDtos().add(iterableResultDto);
    }

    private List<List<TriangleNumber>> fetchMatrix(String file) {
        return readDoubleListListFromFile(file);
    }


    public List<String> readStringListFromFile(String file) {
        List<String> result = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File(file))) {
            scanner.useDelimiter(" ");
            while (scanner.hasNextLine()) {
                result.add(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }

    private List<List<TriangleNumber>> readDoubleListListFromFile(String file) {

        List<List<TriangleNumber>> result = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File(file))) {
            while (scanner.hasNextLine()) {
                result.add(Stream.of(scanner.nextLine().split("\t")).map(tr -> {
                    List<Double> split = Arrays.stream(tr.split(";")).map(str -> Double.valueOf(str))
                            .collect(Collectors.toList());
                    return new TriangleNumber(split.get(0), split.get(1), split.get(2));
                }).collect(Collectors.toList()));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }


    public void save(String c, String v, String l1) {
        try {
            FileWriter writer = new FileWriter("categories-2.txt");
            writer.write(c);
            writer.close();

            writer = new FileWriter("categories-3.txt");
            writer.write(v);
            writer.close();

            writer = new FileWriter("level-1.txt");
            writer.write(l1);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void save(List<String> l2) {
        try {
            for (int i = 0; i < l2.size(); i++) {
                FileWriter writer = new FileWriter("level-2-" + (i + 1) + ".txt");
                writer.write(l2.get(i));
                writer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
