package com.m1.controller;

import com.m1.service.CalculatingService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class ResultController {

    private final CalculatingService service;

    public ResultController(CalculatingService service) {
        this.service = service;
    }

    @GetMapping("/")
    public String result(Model model) {
        model.addAttribute("result", service.calculate());
        return "index";
    }

    @GetMapping("/update")
    public String update(Model model) {

        return "update";
    }


    @PostMapping("/update")
    public String update(Model model, @RequestParam String categories, @RequestParam String varieties,
                         @RequestParam String l1) {
        service.save(categories, varieties, l1);
        return "redirect:update-2";
    }

    @GetMapping("/update-2")
    public String update2(Model model) {
        model.addAttribute("list", service.readStringListFromFile("categories-2.txt"));
        return "update-2";
    }

    @PostMapping("/update-2")
    public String update2(Model model, @RequestParam("l2") List<String> l2) {
        service.save(l2);
        return "redirect:/";
    }
}
