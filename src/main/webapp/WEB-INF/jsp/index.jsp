<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Calculations</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
</head>
<body>
<table class="table table-bordered">
    <tr>
        <th scope="col"></th>
        <th scope="col">Criteria</th>
        <c:forEach items="${result.getCategories()}" var="c">
            <th scope="col">${c}</th>
        </c:forEach>
    </tr>
    <c:forEach items="${result.getCategories()}" var="c">
        <tr>
            <td scope="row">${j=j+1}</td>
            <td>${c} </td>
            <c:forEach items="${result.getIterableResultDtos().get(0).getMatrix().get(j-1)}"
                       var="i">
                <td>${i}</td>
            </c:forEach>
        </tr>

    </c:forEach>
</table>


<table class="table table-bordered">
    <tr>
        <th scope="col">Criteria</th>
        <c:forEach items="${result.getCategories()}" var="c">
            <th scope="col">${c}</th>
        </c:forEach>
    </tr>
    <tr>
        <td>r</td>
        <c:forEach items="${result.getIterableResultDtos().get(0).getR()}"
                   var="i">
            <td>${i}</td>
        </c:forEach>
    </tr>
</table>
<div style="text-align: center">sr = ${result.getIterableResultDtos().get(0).getSr()}</div>
<div style="text-align: center">isr = ${result.getIterableResultDtos().get(0).getIsr()}</div>

<table class="table table-bordered">
    <tr>
        <th scope="col">Criteria</th>
        <c:forEach items="${result.getCategories()}" var="c">
            <th scope="col">${c}</th>
        </c:forEach>
    </tr>
    <tr>
        <td>w</td>
        <c:forEach items="${result.getIterableResultDtos().get(0).getW()}"
                   var="i">
            <td>${i}</td>
        </c:forEach>
    </tr>
</table>

<table class="table table-bordered">
    <tr>
        <th scope="col">Criteria</th>
        <c:forEach items="${result.getCategories()}" var="c">
            <th scope="col">${c}</th>
        </c:forEach>
    </tr>
    <tr>
        <td>M</td>
        <c:forEach items="${result.getIterableResultDtos().get(0).getM()}"
                   var="i">
            <td>${i}</td>
        </c:forEach>
    </tr>
</table>

<table class="table table-bordered">
    <tr>
        <th scope="col">Criteria</th>
        <c:forEach items="${result.getCategories()}" var="c">
            <th scope="col">${c}</th>
        </c:forEach>
        <th>sum N</th>
    </tr>
    <tr>
        <td>N</td>
        <c:forEach items="${result.getIterableResultDtos().get(0).getN()}"
                   var="i">
            <td>${i}</td>
        </c:forEach>
        <td>1</td>
    </tr>
</table>


<c:forEach items="${result.getIterableResultDtos().subList(1, result.getIterableResultDtos().size())}" var="m">
<div style="text-align: center;">${m.getCategory()}</div>
    <table class="table table-bordered">
        <tr>
            <th scope="col"></th>
            <th scope="col">Alternatives</th>
            <c:forEach items="${result.getVariatees()}" var="c">
                <th scope="col">${c}</th>
            </c:forEach>
        </tr>
        <div style="display: none">${j=0}</div>
        <c:forEach items="${result.getVariatees()}" var="c">
            <tr>
                <td scope="row">${j=j+1}</td>
                <td>${c} </td>
                <c:forEach items="${m.getMatrix().get(j-1)}"
                           var="i">
                    <td>${i}</td>
                </c:forEach>
            </tr>

        </c:forEach>
    </table>




    <table class="table table-bordered">
        <tr>
            <th scope="col">Alternatives</th>
            <c:forEach items="${result.getVariatees()}" var="c">
                <th scope="col">${c}</th>
            </c:forEach>
        </tr>
        <tr>
            <td>r</td>
            <c:forEach items="${m.getR()}"
                       var="i">
                <td>${i}</td>
            </c:forEach>
        </tr>
    </table>
    <div style="text-align: center">sr = ${m.getSr()}</div>
    <div style="text-align: center">isr = ${m.getIsr()}</div>

    <table class="table table-bordered">
        <tr>
            <th scope="col">Alternatives</th>
            <c:forEach items="${result.getVariatees()}" var="c">
                <th scope="col">${c}</th>
            </c:forEach>
        </tr>
        <tr>
            <td>w</td>
            <c:forEach items="${m.getW()}"
                       var="i">
                <td>${i}</td>
            </c:forEach>
        </tr>
    </table>

    <table class="table table-bordered">
        <tr>
            <th scope="col">Alternatives</th>
            <c:forEach items="${result.getVariatees()}" var="c">
                <th scope="col">${c}</th>
            </c:forEach>
        </tr>
        <tr>
            <td>M</td>
            <c:forEach items="${m.getM()}"
                       var="i">
                <td>${i}</td>
            </c:forEach>
        </tr>
    </table>

    <table class="table table-bordered">
        <tr>
            <th scope="col">Alternatives</th>
            <c:forEach items="${result.getVariatees()}" var="c">
                <th scope="col">${c}</th>
            </c:forEach>
            <th>sum N</th>
        </tr>
        <tr>
            <td>N</td>
            <c:forEach items="${m.getN()}"
                       var="i">
                <td>${i}</td>
            </c:forEach>
            <td>1</td>
        </tr>
    </table>


</c:forEach>

<table class="table table-bordered">
    <tr>
        <th scope="col" colspan="2">Criteria</th>
        <th colspan="5">Weights of alternatives with respect to related criterion</th>
    </tr>
    <tr>
        <td scope="col"></td>
        <td >Weight</td>
        <c:forEach items="${result.getVariatees()}" var="c">
            <td scope="col">${c}</td>
        </c:forEach>
    </tr>
    <div style="display: none">${j=0}</div>
    <c:forEach items="${result.getCategories()}" var="c">
        <tr>
            <div style="display: none">${j=j+1}</div>
            <td scope="row">${c} </td>
            <td>${result.getIterableResultDtos().get(0).getN().get(j-1)}</td>
            <c:forEach items="${result.getIterableResultDtos().get(j).getN()}"
                       var="i">
                <td>${i}</td>
            </c:forEach>
        </tr>
    </c:forEach>

    <tr>
        <td colspan="2">Total scores:</td>
        <c:forEach items="${result.getTotalScore()}" var="ts">
            <td>${ts}</td>
        </c:forEach>
    </tr>
</table>

<p clas="success" style="text-align: center;">Winner: ${result.getBestResult()}</p>
</body>
</html>
